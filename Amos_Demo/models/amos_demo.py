# -*- coding: utf-8 -*-
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
# odoo16
# QQ:35350428
# 邮件:35350428@qq.com
# 手机：13584935775
# 作者：'Amos'
# 公司网址： www.xodoo.cn
# Copyright 昆山一百计算机有限公司
# 日期：2023-09-16
# &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

import base64
# from docx2pdf import convert
import os
import subprocess
from datetime import datetime, timedelta, date
import time
from docxtpl import DocxTemplate
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import AccessError, UserError, ValidationError
import uuid
from odoo.tools import date_utils


#::::::::::字段属性
"""
size=50                   长度
translate=False           翻译
required=True                   必填
index=True                      索引
default=fields.Datetime.now()   长日期
string='年度'                    字段中文名称
tracking=True                   启用社交后记录字段历史变更信息
copy=False                      数据被复制时是否同时复制当前字段
readonly=True                   视图字段为只读，不可编辑，但如果在视图中定义属性以视图为准
strip_style=True
"""


class amos_demo_tag(models.Model):
    _name = "amos.demo.tag"
    _description = "演示工作流明细"
    _order = 'id desc'

    name = fields.Char(string='标签')
    color = fields.Integer(string='颜色', default=1)


class amos_demo(models.Model):  # 一般与 _name 同名 点换成下划线
    _name = "amos.demo"  # 表名
    _description = "演示工作流"  # 表的中文名称
    _order = 'id desc' #数据排序方式
    _inherit = ['mail.thread', 'mail.activity.mixin']  # 引入社交：可以记录附件，保存历史字段变更信息
    _is_worflow = True  # 启动工作流 需要配合 state 字段

    year = fields.Char(string='年度', required=True, index=True, default=lambda self: fields.datetime.now().year)
    month = fields.Char(string='月度', required=True, index=True, default=lambda self: fields.datetime.now().month)
    hour = fields.Char(string='小时', required=True, index=True, default=lambda self: fields.datetime.now().hour)
    week_start = fields.Char(string='星期几', required=True, default=lambda self: fields.datetime.now().weekday() + 1)
    week = fields.Char(string='在全年多少周', required=True, default=fields.Datetime.now().isocalendar()[1])
    quarter = fields.Char(string='季', required=True,
                          default=lambda self: str(date_utils.get_quarter_number(fields.datetime.now())))
    create_date = fields.Datetime(string='创建时间', required=True, readonly=True, index=True, copy=False,
                                  default=fields.Datetime.now)
    date = fields.Date('截止日期', tracking=True, default=fields.Date.today, required=True)

    top_time = fields.Datetime('置顶', default=fields.Datetime.now())  # 默认当前日期

    @api.onchange('create_date')
    def onchange_create_date(self):
        values = {}
        if self.create_date:
            values['year'] = self.create_date.year
            values['month'] = self.create_date.month
            values['hour'] = self.create_date.hour + 8
            values['week_start'] = self.create_date.weekday() + 1
            values['week'] = self.create_date.isocalendar()[1]
            values['quarter'] = str(date_utils.get_quarter_number(self.create_date))
        self.update(values)

    name = fields.Char(string='项目名称', default='News')
    color = fields.Integer(string='颜色', )
    date_start = fields.Date('客户交期', tracking=True)
    user_id = fields.Many2one('res.users', string='负责人', index=True, tracking=True,
                              default=lambda self: self.env.user, )

    company_id = fields.Many2one('res.company', '公司', required=True, default=lambda s: s.env.company.id, index=True)
    active = fields.Boolean(default=True, string='是否归档', tracking=True)

    tag_ids = fields.Many2many('amos.demo.tag', 'amos_demo_amos_demo_tag_rel', 'demo_id', 'tag_id')

    order_line1 = fields.One2many('amos.demo.line', 'order_id1', string=u'明细行', copy=True)

    note = fields.Text(u'备注', tracking=True)

    sequence = fields.Integer(string='排序', default=50)

    type = fields.Selection([
        ('上架', '上架'),
        ('下架', '下架'),
    ], string='类型', default='上架')

    line_up = fields.Selection([
        ('上海', '上海'),
        ('北京', '北京'),
        ('南京', '南京'),
    ], string='项目城市', default='上海')

    state = fields.Selection([
        ('新建', '新建'),
        ('校对', '校对'),
        ('审核', '审核'),
        ('批准', '批准'),
        ('已发布', '已发布'),
    ], string='状态', readonly=True, copy=False, index=True, tracking=True,
        default='新建')

    def action_confirm(self):
        return True

    def _cron_update_amos_demo(self):
        print('我来了')
        self.env['ir.config_parameter'].set_param('hd', '0000')

    @api.model
    def get_import_templates(self):
        res = super(amos_demo, self).get_import_templates()
        if self.env.context.get('purchase_product_template'):
            return [{
                'label': _('Import Template for Products'),
                'template': '/demo/static/xls/product_purchase.xls'
            }]
        return res


    def create_workflow(self):
            url = '/workflow?model=amos.demo&ttype=create'

            return {
                'type': 'ir.actions.act_url',
                'url': url,
            }



class amos_demo_line(models.Model):
    _name = "amos.demo.line"
    _description = "演示工作流明细"
    _order = 'id desc'

    name = fields.Char(string='项目名称')
    order_id1 = fields.Many2one('amos.demo', string=u'明细', required=True, ondelete='cascade', index=True, copy=False)

    @api.model
    def create(self, vals):
        context = dict(self._context or {})
        print(context)
        line = super(amos_demo_line, self).create(vals)
        return line
