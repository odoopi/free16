

[![源码下载](https://gitee.com/xodoo/free/raw/master/resource/static/src/svg/master.svg)](https://gitee.com/odoo/odoo)
[![技术文档](https://gitee.com/xodoo/free/raw/master/resource/static/src/svg/master-docs.svg?style=flat&colorA=8F8F8F)](https://blog.csdn.net/qq_70140795?spm=1010.2135.3001.5421)
[![视频教程](https://gitee.com/xodoo/free/raw/master/resource/static/src/svg/master-help.svg?style=flat&colorA=8F8F8F)](https://space.bilibili.com/384640736?spm_id_from=333.1007.0.0)
[![免费应用](https://gitee.com/xodoo/free/raw/master/resource/static/src/svg/doc.svg?style=flat&colorA=8F8F8F)](https://gitee.com/xodoo)


<p align="center">
    <img width="200" src="https://gitee.com/xodoo/free/raw/master/resource/static/src/svg/odoo.svg">
</p>
<p align="center">
	<a href="https://gitee.com/xodoo" target="_blank">
		<a href='https://gitee.com/xodoo/free16/members'><img src='https://gitee.com/xodoo/free16/badge/fork.svg?theme=dark' alt='fork'></img></a>
	</a>
	<a href="https://gitee.com/xodoo" target="_blank">
		<a href='https://gitee.com/xodoo/free16/stargazers'><img src='https://gitee.com/xodoo/free16/badge/star.svg?theme=dark' alt='star'></img></a>
	</a>
</p>

<h1 align="center">《Odoo》· 企业一站式解决方案 </h1>

<div align="center">


<p>基于odoo平台，连接千万应用</p>

```
🕙 项目基本保持每周更新，右上随手点个 🌟 Star 关注，这样才有持续下去的动力，谢谢～
```

</div>

</br></br>
### 扫码体验
<img src="https://gitee.com/xodoo/free/raw/master/resource/static/src/img/wxgzh.png" width="100%" />

# 开源市场

> 首页：http://xodoo.cn



### 免费模块
功能如下：如果ID号相同说明是依赖关系
| 序号 |    模块名称   |中文名称                          |查看|星级
|:----------------:|:-------------------------------|-----------------------------|:-----------------------------:|:-----------------------------|
|1|InstallScript|`系统安装的脚本`            |<a href="https://gitee.com/xodoo/free16/tree/master/InstallScript">查看</a>            |⭐️⭐️⭐️⭐️⭐️|
|2|sidebar          |`侧边栏`            |<a href="https://gitee.com/xodoo/free16/tree/master/sidebar">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|3|web_help          |`指导帮助`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_help">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|4|web_action_conditionable          |`指定状态下创建与修改也支持一对多`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_action_conditionable">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|5|web_advanced_search          |`扩展搜索`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_advanced_search">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|6|web_apply_field_style          |`定义字段颜色`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_apply_field_style">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|7|web_chatter_position          |`聊天位置选项`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_chatter_position">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|8|web_company_color          |`根据Logo颜色修改导航颜色`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_company_color">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|9|web_dark_mode          |`暗黑模式`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_dark_mode">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|10|web_dialog_size          |`弹出窗口是否最大化`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_dialog_size">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|11|web_domain_field          |`字段过滤规则扩展`            |<a href="https://gitee.com/xodoo/free16/tree/master/web_domain_field">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|12|web_environment_ribbon          |`在每页的左上角用红丝带标记测试环境`       |<a href="https://gitee.com/xodoo/free16/tree/master/web_environment_ribbon">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|13|web_field_numeric_formatting          |`数字字段格式化`       |<a href="https://gitee.com/xodoo/free16/tree/master/web_field_numeric_formatting">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|14|web_group_expand          |` web组展开 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_group_expand">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|15|web_ir_actions_act_multi          |` 触发多个操作 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_ir_actions_act_multi">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|16|web_ir_actions_act_window_page          |` 允许开发人员返回以下操作类型 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_ir_actions_act_window_page">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|17|web_listview_range_select          |` 允许使用shift键选择一系列记录 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_listview_range_select">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|18|web_m2x_options          |` “many2one”和“many2manytags”窗体小部件，添加一些新的显示控制选项 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_m2x_options">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|19|web_no_bubble          |` “气泡 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_no_bubble">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|20|web_notify          |` “实时向用户发送即时通知消息 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_notify">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|21|web_refresher          |` “刷新列表 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_refresher">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|22|web_remember_tree_column_width          |` “记住树列表宽度 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_remember_tree_column_width">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|23|web_responsive          |` “全屏应用 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_responsive">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|24|web_save_discard_button          |` “保存放弃按钮 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_save_discard_button">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|25|web_search_with_and          |` 搜索中使用AND条件 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_search_with_and">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|26|web_select_all_companies          |` 允许您在一次单击中选择或取消选择所有公司 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_select_all_companies">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|27|web_sheet_full_width          |` 网页全宽 `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_sheet_full_width">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|28|web_theme_classic          |` Web主题经典  `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_theme_classic">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|29|web_timeline          |`  Web时间线   `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_timeline">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|30|web_tree_many2one_clickable          |`  many2one字段进入视图   `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_tree_many2one_clickable">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|31|web_widget_bokeh_chart          |`  小部件图表   `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_widget_bokeh_chart">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|32|web_widget_dropdown_dynamic          |`   动态下拉小部件   `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_widget_dropdown_dynamic">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|33|web_widget_numeric_step          |`   数字增减小部件   `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_widget_numeric_step">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|34|web_widget_open_tab          |`   新窗口打开   `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_widget_open_tab">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|35|web_widget_plotly_chart          |`   Web小部件绘图    `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_widget_plotly_chart">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|36|web_widget_x2many_2d_matrix          |`    显示具有3个元组    `       |<a href="https://gitee.com/xodoo/free16/tree/master/web_widget_x2many_2d_matrix">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|accounting_pdf_reports          |`   会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/accounting_pdf_reports">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_account_accountant          |`   会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_account_accountant">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_account_asset          |`   会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_account_asset">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_account_bank_statement_import          |`    会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_account_bank_statement_import">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_account_budget          |`    会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_account_budget">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_account_daily_reports          |`    会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_account_daily_reports">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_account_followup          |`    会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_account_followup">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_fiscal_year          |`    会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_fiscal_year">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|37|om_recurring_payments          |`    会计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/om_recurring_payments">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|38|base_report_to_printer          |`    PDF打印    `       |<a href="https://gitee.com/xodoo/free16/tree/master/base_report_to_printer">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|38|printing_simple_configuration          |`    PDF打印    `       |<a href="https://gitee.com/xodoo/free16/tree/master/printing_simple_configuration">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|38|printer_zpl2          |`    PDF打印    `       |<a href="https://gitee.com/xodoo/free16/tree/master/printer_zpl2">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|39|workflow          |`    工作流设计    `       |<a href="https://gitee.com/xodoo/free16/tree/master/workflow">查看</a>           |⭐️⭐️⭐️⭐️⭐️|
|39|workflow_sale          |`    工作流设计-销售审核    `       |<a href="https://gitee.com/xodoo/free16/tree/master/workflow_sale">查看</a>           |⭐️⭐️⭐️⭐️⭐️|



### 软件版本
|    数据源     |   版本    | 下载 | 文档  |
|:----------:|:-------:|:--:|:---:|
| Python |  3.10   | <a href="https://www.python.org/">下载</a> | <a href="https://www.runoob.com/python3/python3-tutorial.html">Python 3 教程</a>  |
| PostGreSQL |   15    | <a href="https://www.postgresql.org/download/">下载</a> | <a href="https://www.postgresql.org/docs/15/index.html">PostgreSQL 15</a>  |
|  Pycharm   |   最新版   | <a href="https://www.jetbrains.com/pycharm/">下载</a> | |
|    浏览器     | 支持html5 |  |  |


### 项目成员

**创始人**
[(Amos)](https://gitee.com/amoserp)

如何成为外部贡献者？ 提交有意义的PR，并被采纳。

